package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

func getManifest() (man Manifest) {
	fil, err := ioutil.ReadFile("qbspm.yaml")
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal([]byte(fil), &man)
	if err != nil {
		panic(err)
	}
	return
}
