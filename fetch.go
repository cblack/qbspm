package main

import (
	"os"
	"os/exec"

	"github.com/urfave/cli/v2"
)

func Fetch(c *cli.Context) error {
	pkgs := getManifest()
	for _, pkg := range pkgs.Packages {
		cmd := exec.Command("go", "get", "-d", "-v", pkg.Name+"@"+pkg.Version)
		cmd.Stderr = os.Stderr
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Env = os.Environ()
		cmd.Env = append(cmd.Env, "GO11MODULES=on")
		err := cmd.Run()
		if err != nil {
			panic(err)
		}
	}
	return nil
}
