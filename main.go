package main

import (
	"os"

	"github.com/urfave/cli/v2"
)

type Package struct {
	Name    string `yaml:"name"`
	Version string `yaml:"version"`
}

type Manifest struct {
	Packages []Package `yaml:"dependencies"`
}

func main() {
	(&cli.App{
		Usage: "qbs package manager",
		Commands: []*cli.Command{
			{
				Name:   "link",
				Usage:  "link packages from the cache into the project",
				Action: Link,
			},
			{
				Name:   "fetch",
				Usage:  "download packages from the manifest",
				Action: Fetch,
			},
		},
	}).Run(os.Args)
}
