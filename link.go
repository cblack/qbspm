package main

import (
	"go/build"
	"os"
	"path"
	"path/filepath"

	"github.com/urfave/cli/v2"
)

func packagePath(p Package) string {
	return path.Join(build.Default.GOPATH, "pkg", "mod", p.Name+"@"+p.Version)
}

func packagePathToLink(pkg Package) string {
	return path.Join(".", ".qbspm", "modules", pkg.Name)
}

func Link(c *cli.Context) error {
	man := getManifest()
	for _, pkg := range man.Packages {
		it := packagePathToLink(pkg)
		err := os.MkdirAll(filepath.Dir(it), os.ModePerm)
		if err != nil {
			panic(err)
		}

		err = os.Symlink(packagePath(pkg), it)
		if err != nil && !os.IsExist(err) {
			panic(err)
		}
	}
	return nil
}
